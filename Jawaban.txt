1. Buat Database
CREATE DATABASE myshop;

2. Membuat Table users
- CREATE TABLE users( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

Membuat Table categories
- CREATE TABLE categories(id int AUTO_INCREMENT PRIMARY KEY, name(255) NOT null );

Membuat Table Items
- CREATE TABLE items( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, categories_id int NOT null, FOREIGN KEY(categories_id) REFERENCES categories(id) );

3. Memasukkan Data pada Table
-INSERT INTO users(name,email,password) values("John Doe","john@doe,com","john123"),("Jane Doe","jane@doe.com","jenita123");
-INSERT INTO categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
-INSERT INTO items(name,description,price,stock,categories_id)VALUES("Sumsang b50","hape keren dari merek sumsang","4000000","100","1"),("Uniklooh","baju keren dari bran ternama","500000","50","2"),("IMHO Watch","Jam tangan anak yang jujur banget","2000000","10","1");

4.Mengambil Data dari Database
a.) Mengambil data users
-SELECT id , name, email from users;

b.) Mengambil Data items
- Point 1 : SELECT * from items where price > 1000000;
- Point 2 (menggunakan LIKE): SELECT * FROM items WHERE name LIKE '%Watch';

c.) Menampilkan data items join dengan categories
-SELECT items.name, items.description, items.price, items.stock, items.categories_id, categories.name AS kategori from items INNER JOIN categories on items.categories_id = categories.id;

5. Mengubah Data dari DATABASE
-UPDATE items SET price=2500000 where name = 'Sumsang b50';